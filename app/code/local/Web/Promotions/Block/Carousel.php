<?php

class Web_Promotions_Block_Carousel extends Mage_Core_Block_Template
{

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getPromotionsCollection()
    {
        return  Mage::getModel('webpromotions/promotions')->getCollection();
    }
}
