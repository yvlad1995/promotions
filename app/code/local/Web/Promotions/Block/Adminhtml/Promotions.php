<?php

class Web_Promotions_Block_Adminhtml_Promotions extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    
    protected function _construct() 
    {
        parent::_construct();
        
        $helper = Mage::helper('webpromotions');
        $this->_blockGroup = 'webpromotions';
        $this->_controller = 'adminhtml_promotions';
        
        $this->_headerText = $helper->__('Promotions');
        $this->_addButtonLabel = $helper->__('Add New');
    }
  
}