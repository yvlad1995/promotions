<?php


class Web_Promotions_Block_Adminhtml_Promotions_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct() {
        //инициализируется блок редактирования новости, в конструкторе которого происходит инициализация переменных формы,
        // которые будут использоваться для построения класса самой формы по схеме [_blockGroup]/[_controller]_[_mode]_form, 
        // в результате получится dsnews/adminhtml_news_edit_form (_mode по умолчанию имеет значение edit).
        $this->_blockGroup = 'webpromotions';//устанавливаем блок группу
        $this->_controller = 'adminhtml_promotions'; //устанавливаем контроллер для обработки/вывода блока
    }
    
    public function getHeaderText() {
        $helper = Mage::helper('webpromotions');
        $model  = Mage::registry('current_promotions');
        
        if($model->getId()){
            return $helper->__("Edit Promotion item '%s'", $this->escapeHtml($model->getPromotionsName()));
        }else{
            return $helper->__("New Menu");
        }
    }
}