<?php

class Web_Promotions_Block_Adminhtml_Promotions_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()

    {
        $helper = Mage::helper('webpromotions');
        $model  = Mage::registry('current_promotions');

        $form = new Varien_Data_Form(array(
            'id'     => 'edit_form',
            'action' => $this->getUrl('*/*/save', array(
                'id'     => $this->getRequest()->getParam('id')
            )),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));
        $fieldset = $form->addFieldset('general_form', array(
            'legend'  => $helper->__('Promotion Information')
        ));

        $fieldset->addField('promotions_name', 'text', array(
            'label'    => $helper->__('Promotion Name'),
            'required' => true,
            'name'     => 'promotions_name',
        ));

        $fieldset->addField('is_enabled', 'select', array(
            'label'   => $helper->__('Promotions Enabled?'),
            'name'    => 'is_enabled',
            'required'=> true,
            'values'  => Mage::getSingleton('adminhtml/system_config_source_enabledisable')->toOptionArray()
        ));


        $fieldset->addField('description', 'textarea', array(
            'label'    => $helper->__('Description'),
            'default'  => 0,
            'name'     => 'description',
            'required' => true,
        ));

        $fieldset->addField('static_blocks', 'multiselect', array(
            'label'   => $helper->__('Static Blocks'),
            'name'    => 'static_blocks[]',
            'required'=> true,
            'values'  => Mage::getModel('webpromotions/block')->toOptionArray('cms/block'),
        ));

        $fieldset->addField('category', 'multiselect', array(
            'label'   => $helper->__('Category'),
            'name'    => 'category[]',
            'required'=> true,
            'values'  => Mage::getModel('webpromotions/block')->toOptionArray('catalog/category'),
        ));

//        $fieldset->addField('products', 'multiselect', array(
//            'label'   => $helper->__('Products'),
//            'name'    => 'products[]',
//            'values'  => Mage::getModel('webpromotions/block')->toOptionArray('catalog/product')
//        ));

//        $fieldset->addField('sort_order', 'text', array(
//            'label'   => $helper->__('Sort Order'),
//            'name'    => 'sort_order',
//            'default' => '0'
//        ));

        $fieldset->addField('image', 'image', array(
            'label'   => $helper->__('Image'),
            'name'    => 'image',
            'note' => $helper->__('Add images format jpeg/jpg'),
        ));

        $formData = array_merge($model->getData(), array('image' => $model->getImageUrl()));
        $form->setUseContainer(true);
        $form->setValues($formData);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}