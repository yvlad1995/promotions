<?php

class Web_Promotions_Adminhtml_PromotionsController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('webpromotions');
        $this->_addContent($this->getLayout()->createBlock('webpromotions/adminhtml_promotions'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        $model = Mage::getModel('webpromotions/promotions');
        $data = Mage::getSingleton('adminhtml/session')->getFormData();

        if($data){
            $model->setData($data)->setId($id);
        }else{
            $model->load($id);
            $this->getDataUnserialize($model);
        }

        Mage::register('current_promotions', $model);

        $this->loadLayout()->_setActiveMenu('webpromotions');
        $this->_addContent($this->getLayout()->createBlock('webpromotions/adminhtml_promotions_edit'));

        $this->renderLayout();
    }

    public function saveAction()
    {
        if($data = $this->getRequest()->getPost())
        {
            try{
                $helper = Mage::helper('webpromotions');
                $model = Mage::getModel('webpromotions/promotions');
                $dataPost = $this->getDataPost($data);
                
                $model->setData($dataPost)->setId($this->getRequest()->getParam('id'));

                $model->save();
                $id = $model->getId();

                if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '' ) {
                    $image = Mage::helper('webpromotions/image');
                    $image->setId($id);
                    $image->imageBaseSave();
                    $model->setImage($helper->getImageUrl($id));
                    $model->save()->setId('id');
                }

                if (isset($data['image']['delete']) && $data['image']['delete'] == 1 && file_exists($helper->getImagePath($id))) {
                    @unlink($helper->getImagePath($id));
                    $model->setImage('');
                    $model->save()->setId($id);
                }

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Promotion \'%s\' was succesfuly saved', $model->getPromotionsName()));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                $this->_redirect('*/*/');

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);

                $this->_redirect('*/*/edit' , array(
                    'id'    => $this->getRequest()->getParam('id'),
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));

        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if($id = $this->getRequest()->getParam('id')){
            try{
                $helper = Mage::helper('webpromotions');

                Mage::getModel('webpromotions/promotions')->setId($id)->delete();
                @unlink($helper->getImagePath($id));
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Promotion was deleted successfully'));
            } catch (Exception $ex) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $promotions = $this->getRequest()->getParam('promotions', null);

        if(is_array($promotions) && sizeof($promotions) > 0){
            try {
                $helper = Mage::helper('webpromotions');
                foreach ($promotions as $id){
                    Mage::getModel('webpromotions/promotions')->setId($id)->delete();
                    @unlink($helper->getImagePath($id));
                }
                $this->_getSession()->addSuccess($this->__('Total of %d promotions have been deleted', sizeof($promotions)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select promotions'));
        }
        $this->_redirect('*/*');
    }

    protected function getDataPost($data)
    {

       return array(
            'form_key'        => $data['form_key'],
            'promotions_name' => $data['promotions_name'],
            'description'     => $data['description'],
            'is_enabled'      => $data['is_enabled'],
//            'sort_order'      => $data['sort_order'],
            'static_blocks'   => serialize($data['static_blocks']),
            'category'        => serialize($data['category']),
//            'products'        => serialize($data['products'])
        );
    }

    protected function getDataUnserialize($model)
    {
        $statickBlocks = unserialize($model->getData('static_blocks'));
        $category      = unserialize($model->getData('category'));
//        $products      = unserialize($model->getData('products'));
        return $model->setStaticBlocks($statickBlocks)->setCategory($category);//->setProducts($products);
    }
//    protected function _isAllowed()
//    {
//        switch ($this->getRequest()->getActionName()) {
//            case 'new':
//            case 'save':
//                return Mage::getSingleton('admin/session')->isAllowed('cms/page/save');
//                break;
//            case 'delete':
//                return Mage::getSingleton('admin/session')->isAllowed('cms/page/delete');
//                break;
//            default:
//                return Mage::getSingleton('admin/session')->isAllowed('cms/page');
//                break;
//        }
//    }
}