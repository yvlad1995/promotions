<?php

class Web_Promotions_Helper_Front
{
    public function getPromotionsData($id)
    {
        try{

            $promotions   = Mage::getModel('webpromotions/promotions')->load($id)->getData();
            $staticBlocks = unserialize($promotions['static_blocks']);
            $category     = unserialize($promotions['category']);

            $data = array(
                'id'              => $promotions['id'],
                'promotions_name' => $promotions['promotions_name'],
                'description'     => $promotions['description'],
                'static_blocks'   => $staticBlocks,
                'category'        => $category
            );

            return $data;
        }catch(Exception $e){
            return false;
        }

    }
}
