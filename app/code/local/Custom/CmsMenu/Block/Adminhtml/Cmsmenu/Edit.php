<?php


class Custom_CmsMenu_Block_Adminhtml_Cmsmenu_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct() {
        //инициализируется блок редактирования новости, в конструкторе которого происходит инициализация переменных формы,
        // которые будут использоваться для построения класса самой формы по схеме [_blockGroup]/[_controller]_[_mode]_form, 
        // в результате получится dsnews/adminhtml_news_edit_form (_mode по умолчанию имеет значение edit).
        $this->_blockGroup = 'customcmsmenu';//устанавливаем блок группу
        $this->_controller = 'adminhtml_cmsmenu'; //устанавливаем контроллер для обработки/вывода блока

        if($this->getRequest()->getParam('id')) {

            $this->addButton('addmenulink', array(
                'label' => Mage::helper('customcmsmenu')->__('Add Menu Link'),
                'onclick' => 'addMenuLink()',
                'class' => 'addMenu'
            ), -100);
    //Переписать!
            $this->_formScripts[] =
                "function addMenuLink(){
                     editForm.submit($('edit_form').action+'newlink/edit/');
                }";
        }

        $this->addButton('savaandcontinue', array(
            'label'   => Mage::helper('customcmsmenu')->__('Save and Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class'   => 'save'
        ), -100);
        
        $this->_formScripts[] = 
                 "function saveAndContinueEdit(){
                     editForm.submit($('edit_form').action+'back/edit/');
                }";
    }
    
    public function getHeaderText() {
        $helper = Mage::helper('customcmsmenu');
        $model  = Mage::registry('current_cmsmenu');
        
        if($model->getId()){
            return $helper->__("Edit Menu item '%s'", $this->escapeHtml($model->getMenuName()));
        }else{
            return $helper->__("New Menu");
        }
    }
}