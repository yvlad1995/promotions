<?php

/**
 * Class Mage_Adminhtml_Block_Widget_Form_Container
 */

class Custom_CmsMenu_Block_Adminhtml_Cmslinks_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'customcmslinks';
        $this->_controller = 'adminhtml_cmslinks';

        $this->_addButton('my_back', array(
            'label'     => Mage::helper('adminhtml')->__('Back'),
            'onclick'   => "setLocation(" . '\'' .$this->getBackUrl() . 'menu_id/' .
                $this->getRequest()->getParam('menu_id') . '\'' . ")",
            'class'     => 'back',
        ), -1);

        $this->_addButton('delete', array(
            'label'     => Mage::helper('adminhtml')->__('Delete'),
            'class'     => 'delete',
            'onclick'   => 'deleteConfirm(\''
                . Mage::helper('core')->jsQuoteEscape(
                    Mage::helper('adminhtml')->__('Are you sure you want to do this?')
                )
                .'\', \''
                . $this->getDeleteUrl()
                . 'link_id/' . $this->getRequest()->getParam('link_id') . '/menu_id/' . $this->getRequest()->getParam('menu_id') . '\')',
        ));
    }

    protected function _prepareLayout()
    {
        $this->_removeButton('back');

        return parent::_prepareLayout();
    }
    
    public function getHeaderText()
    {
        $helper = Mage::helper('customcmsmenu');
        $model  = Mage::registry('current_cmslinks');
        
        //Все что ниже переписать!!!
        $model = Mage::getModel('customcmsmenu/cmslinks')
                ->getCollection()
                ->addFieldToFilter('links_id', $this->getRequest()->getParam('link_id'))->getData();
        
        if($this->getRequest()->getParam('link_id')){
            return $helper->__("Edit Link item '%s'", $this->escapeHtml($model[0]['label']));//Получаем вывод в шапке редактирования меню
        }else{
            return $helper->__("New Link");//Получаем вывод в шапке Добавления меню
        }
    }

    public function getNotice()
    {
        $this->setTemplate('customcmsmenu/editlink/linknotice.phtml');
    }
}