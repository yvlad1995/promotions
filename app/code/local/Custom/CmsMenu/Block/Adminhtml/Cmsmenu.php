<?php

class Custom_CmsMenu_Block_Adminhtml_Cmsmenu extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    
    protected function _construct() 
    {
        parent::_construct();
        
        $helper = Mage::helper('customcmsmenu'); //Инициализируем хелпер
        $this->_blockGroup = 'customcmsmenu'; // Блок группа
        $this->_controller = 'adminhtml_cmsmenu'; //Путь контроллера controllers/adminhtml/CmsmenuController.php
        
        $this->_headerText = $helper->__('Manage Menus');// Текст хедера в модуле Cms Menu
        $this->_addButtonLabel = $helper->__('Add New'); //Добавляем кнопку добавления Менюшек
    }
  
}