<?php

class Custom_CmsMenu_Adminhtml_CmsmenuController extends Mage_Adminhtml_Controller_Action
{
    
    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('cms');
        $this->_addContent($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu'));
        $this->renderLayout();
    }

    public function lookGridAction()
    {
        $output = $this->getLayout()->createBlock('adminhtml/text_list');

        $gridBlock = $this->getLayout()->createBlock('customcmsmenu/adminhtml_cmslinks_grid');

        $formKey = $this->getLayout()->createBlock('core/template')->setTemplate('formkey.phtml');

        $gridBlock->setChild('formkey', $formKey);

        $this->getLayout()->setBlock('formkey', $formKey);

        $serializer = $this->getLayout()->createBlock('adminhtml/widget_grid_serializer');
        $serializer->initSerializerBlock($gridBlock, 'getSelectedLinks', 'menu', 'menu');

        $output->insert($gridBlock, '', true);
        $output->insert($serializer, '', true);

        $this->getResponse()->setBody($output->toHtml());
    }

    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName()) {
            case 'new':
            case 'save':
                return Mage::getSingleton('admin/session')->isAllowed('cms/page/save');
                break;
            case 'delete':
                return Mage::getSingleton('admin/session')->isAllowed('cms/page/delete');
                break;
            default:
                return Mage::getSingleton('admin/session')->isAllowed('cms/page');
                break;
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        $model = Mage::getModel('customcmsmenu/cmsmenu');

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $model->setData($data)->setId($id); 
        }else{
            $model->load($id);
        }
       
        Mage::register('current_cmsmenu', $model);

        $this->loadLayout()->_setActiveMenu('cms');
        $this->_addLeft($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu_edit_tabs'));
        $this->_addContent($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu_edit'));

        $this->renderLayout();
    }
   
    public function saveAction()
    {
        if($data = $this->getRequest()->getPost())
        {
            try{
                $model = Mage::getModel('customcmsmenu/cmsmenu');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));

                if(!$model->getCreated()){
                    $model->setCreated(now());
                }else{
                    $model->setUpdated(now());
                }

                //Переписать
                if($this->getRequest()->getParam('newlink')){
                    return $this->_redirect('*/adminhtml_cmslinks/edit/', array(
                            'menu_id' => $model->getId()
                    ));
                }
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Menu \'%s\' was succesfuly saved', $model->getMenuName()));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if($this->getRequest()->getParam('back')){
                    return $this->_redirect('*/adminhtml_cmsmenu/edit/' , array(
                        'id' => $model->getId()
                    ));
                }
                $this->_redirect('*/*/');

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
               
                $this->_redirect('*/*/edit' , array(
                    'id'    => $this->getRequest()->getParam('id'),
                ));
            }
            return;
        }
        Mage::getSinglton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        
        $this->_redirect('*/*/');   
    }
    
    public function deleteAction()
    {
        if($id = $this->getRequest()->getParam('id')){ 
            try{
                Mage::getModel('customcmsmenu/cmsmenu')->setId($id)->delete();
                Mage::getSingleton('customcmsmenu/session')->addSuccess($this->__('Menu was deleted successfully'));
            } catch (Exception $ex) {
                Mage::getSingleton('customcmsmenu/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
    }
    
    public function massDeleteAction()
    {
        $menu = $this->getRequest()->getParam('menu', null);
        
        if(is_array($menu) && sizeof($menu) > 0){
            try {
                foreach ($menu as $id){
                    Mage::getModel('customcmsmenu/cmsmenu')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d menu have been deleted', sizeof($menu)));
            } catch (Exception $e) {
                $this->_getSesion()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select menu'));
        }
        $this->_redirect('*/*');
    }
}