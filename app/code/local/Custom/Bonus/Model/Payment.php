<?php

class Custom_Bonus_Model_Payment extends Mage_Payment_Model_Method_Abstract
{
    protected $_code                    = 'custombonus';
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canRefund               = true;
    protected $_canRefundInvoicePartial = true;
//    protected $_canOrder                = true;

    public function refund(Varien_Object $payment, $amount)
    {
        $customer_id = $this->getCustomerId($payment);
        $this->getBonusCustomer()->editBonus($customer_id, $amount, true);

        return $this;
    }

    public function capture(Varien_Object $payment, $amount)
    {
        $customer_id = $this->getCustomerId($payment);
        $this->getBonusCustomer()->editBonus($customer_id, $amount);
        
        $payment->setTransactionId(time());

        return $this;
    }

    public function isApplicableToQuote($quote, $checksBitMask)
    {
        if ($checksBitMask & self::CHECK_USE_FOR_COUNTRY) {
            if (!$this->canUseForCountry($quote->getBillingAddress()->getCountry())) {
                return false;
            }
        }
        if ($checksBitMask & self::CHECK_USE_FOR_CURRENCY) {
            if (!$this->canUseForCurrency($quote->getStore()->getBaseCurrencyCode())) {
                return false;
            }
        }
        if ($checksBitMask & self::CHECK_USE_CHECKOUT) {
            if (!$this->canUseCheckout()) {
                return false;
            }
        }
        if ($checksBitMask & self::CHECK_USE_FOR_MULTISHIPPING) {
            if (!$this->canUseForMultishipping()) {
                return false;
            }
        }
        if ($checksBitMask & self::CHECK_USE_INTERNAL) {
            if (!$this->canUseInternal()) {
                return false;
            }
        }
        if ($checksBitMask & self::CHECK_ORDER_TOTAL_MIN_MAX) {
            $total = $quote->getBaseGrandTotal();
            $minTotal = $this->getConfigData('min_order_total');
            $maxTotal = $this->getConfigData('max_order_total');
            if (!empty($minTotal) && $total < $minTotal || !empty($maxTotal) && $total > $maxTotal) {
                return false;
            }
        }
        if ($checksBitMask & self::CHECK_RECURRING_PROFILES) {
            if (!$this->canManageRecurringProfiles() && $quote->hasRecurringItems()) {
                return false;
            }
        }
        if ($checksBitMask & self::CHECK_ZERO_TOTAL) {
            $total = $quote->getBaseSubtotal() + $quote->getShippingAddress()->getBaseShippingAmount();
            if ($total < 0.0001 && $this->getCode() != 'free'
                && !($this->canManageRecurringProfiles() && $quote->hasRecurringItems())
            ) {
                return false;
            }
        }

        $total = $quote->getBaseSubtotal() + $quote->getShippingAddress()->getBaseShippingAmount();
        if(empty($this->getBonusCustomer()->getBonus($this->getCustomerId())) || $this->getBonusCustomer()->getBonus($this->getCustomerId()) < $total)
        {
            return false;
        }

        return true;
    }

    protected function getCustomerId(Varien_Object $payment = null)
    {
        if(empty($payment))
        {
            return Mage::getSingleton('customer/session')->getId();
        }

        return $payment->getOrder()->getData('customer_id');
    }

    protected function getBonusCustomer()
    {
        return Mage::getSingleton('custombonus/bonus');
    }

}




